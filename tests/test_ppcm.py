import subprocess

executable = "build/ppcm"


def test_ppcm_with_0_args():

    cp = subprocess.run(
        [executable],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode != 0
    assert cp.stdout != ""


def test_ppcm_with_1_args():

    cp = subprocess.run(
        [executable, "1"],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode == 0
    assert cp.stdout == "1\n"


def test_ppcm_with_2_args():

    cp = subprocess.run(
        [executable, "12", "4"],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode == 0
    assert cp.stdout == "12\n"


def test_ppcm_with_4_identical_args():

    cp = subprocess.run(
        [executable, "12", "12", "12", "12"],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode == 0
    assert cp.stdout == "12\n"


def test_ppcm_with_3_args():

    cp = subprocess.run(
        [executable, "12", "6", "15"],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode == 0
    assert cp.stdout == "60\n"


def test_ppcm_with_9_args():

    cp = subprocess.run(
        [executable, "12", "34", "54", "72", "28", "72", "81", "18", "26"],
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert cp.returncode == 0
    assert cp.stdout == "1002456\n"
