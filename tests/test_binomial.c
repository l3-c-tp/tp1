#include <stdio.h>

#include "func_binomial.h"

#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

void test_rec_binomial() {

    assert_int_equal(rec_binomial(0, 0), 1);
    assert_int_equal(rec_binomial(1, 0), 1);
    assert_int_equal(rec_binomial(1, 1), 1);
    assert_int_equal(rec_binomial(10, 0), 1);
    assert_int_equal(rec_binomial(10, 10), 1);
    assert_int_equal(rec_binomial(42, 0), 1);
    assert_int_equal(rec_binomial(42, 42), 1);
    assert_int_equal(rec_binomial(136, 0), 1);
    assert_int_equal(rec_binomial(136, 136), 1);

    assert_int_equal(rec_binomial(7, 3),
                     rec_binomial(6, 3) + rec_binomial(6, 2));
    assert_int_equal(rec_binomial(5, 2),
                     rec_binomial(4, 2) + rec_binomial(4, 1));
}

void test_rec_binomial_complexity() {

    int counter = 0;
    rec_binomial_complexity(0, 0, &counter);
    assert_int_equal(counter, 1);

    counter = 0;
    rec_binomial_complexity(12, 12, &counter);
    assert_int_equal(counter, 1);

    counter = 0;
    rec_binomial_complexity(7, 0, &counter);
    assert_int_equal(counter, 1);

    counter = 0;
    rec_binomial_complexity(5, 2, &counter);
    /* intuition 2**5 (because complexity = 2**n) but the exact formula is more
     * complex. */
    assert_int_equal(counter, 19);
}

void test_array_binomial() {

    assert_int_equal(array_binomial(0, 0), 1);
    assert_int_equal(array_binomial(1, 0), 1);
    assert_int_equal(array_binomial(1, 1), 1);
    assert_int_equal(array_binomial(10, 0), 1);
    assert_int_equal(array_binomial(10, 10), 1);
    assert_int_equal(array_binomial(42, 0), 1);
    assert_int_equal(array_binomial(42, 42), 1);
    assert_int_equal(array_binomial(136, 0), 1);
    assert_int_equal(array_binomial(136, 136), 1);

    assert_int_equal(array_binomial(7, 3),
                     array_binomial(6, 3) + array_binomial(6, 2));
    assert_int_equal(array_binomial(5, 2),
                     array_binomial(4, 2) + array_binomial(4, 1));
}

int main() {

    test_rec_binomial();
    test_rec_binomial_complexity();
    test_array_binomial();
    printf("ok\n");
    return 0;
}