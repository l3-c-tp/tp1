from subprocess import Popen, PIPE, STDOUT
from tempfile import NamedTemporaryFile


executable = "build/mytee"
test_string = b"something to test"


def test_mytee_write_input_to_output():
    p = Popen([executable], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    stdout_data = p.communicate(input=test_string)[0]
    assert stdout_data == test_string


def test_mytee_write_input_to_file():
    tempf = NamedTemporaryFile()
    p = Popen([executable, tempf.name], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    stdout_data = p.communicate(input=test_string)[0]
    assert tempf.read() == test_string
    assert stdout_data == test_string
