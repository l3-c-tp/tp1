#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

/* A test case that does nothing and succeeds. */
static void null_test_success(void **state) { (void)state; /* unused */ }

int f() { return 12; }
int g() { return 12; }

static void test_success_assert(void **state) {
    assert_true(1);
    assert_false(0);
    assert_string_equal("lol", "lol");
    assert_int_equal(f(), g());
}

int main(void) {

    const struct CMUnitTest tests[] = {cmocka_unit_test(null_test_success),
                                       cmocka_unit_test(test_success_assert)};

    return cmocka_run_group_tests(tests, NULL, NULL);
}