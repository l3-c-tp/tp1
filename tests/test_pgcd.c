#include "pgcd.h"
#include <stdio.h>

#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>

#include <cmocka.h>

int main() {

    assert_int_equal(pgcd(1, 1), 1);
    assert_int_equal(pgcd(8, 12), 4);
    assert_int_equal(pgcd(4, 30), 2);
    assert_int_equal(pgcd(42, 9), 3);
    assert_int_equal(pgcd(5, 15), 5);
    assert_int_equal(pgcd(21, 14), 7);
    printf("ok\n");

    return 0;
}