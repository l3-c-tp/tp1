#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

    char input;
    FILE *ptr_file = NULL;

    if (2 == argc) {

        ptr_file = fopen(argv[1], "w");
    }

    const int output_file_exists = (NULL != ptr_file);

    while (scanf("%c", &input) != EOF) {

        printf("%c", input);

        if (output_file_exists) {

            fprintf(ptr_file, "%c", input);
        }
    }

    if (output_file_exists) {

        fclose(ptr_file);
    }

    return EXIT_SUCCESS;
}