int pgcd(int a, int b) {

    if (a == b) {

        return a;
    }

    int c;
    do {

        c = a % b;
        if (0 != c) {
            a = b;
            b = c;
        }

    } while (c != 0);

    return b;
}