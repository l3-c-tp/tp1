#include "func_binomial.h"
#include <assert.h>
#include <stdio.h>

int main() {

    printf("Choisir la méthode récursive [1]\n");
    printf("Choisir la methode avec tableau d'entiers [2]\n");
    int choice = 0;
    scanf("%d", &choice);
    assert(choice > 0);
    assert(choice < 3);

    int n = 0;
    int p = 0;
    int res = 0;
    printf("Choisir n'importe quel entier >= 0 : \n");
    scanf("%d", &n);

    printf("choisir un entier <= %d : \n", n);
    scanf("%d", &p);

    assert(n >= p);
    assert(p >= 0);

    if (choice == 1) {

        res = rec_binomial(n, p);
    } else {

        res = array_binomial(n, p);
    }

    printf("b(%d, %d) = %d\n", n, p, res);

    return 0;
}
