#include "func_binomial.h"
#include <stdlib.h>

int rec_binomial(int const n, int const p) {
    return rec_binomial_complexity(n, p, NULL);
}

int rec_binomial_complexity(int const n, int const p, int *const counter) {
    /* Complexity = 2**n */

    if (counter != NULL) {

        *counter += 1;
    }
    if (0 == p) {

        return 1;
    }
    if (n == p) {

        return 1;
    }

    return rec_binomial_complexity(n - 1, p, counter) +
           rec_binomial_complexity(n - 1, p - 1, counter);
}

int compute_index(int const array_width, int const i, int const j) {

    return array_width * i + j;
}

int array_binomial(int const n, int const p) {

    int const array_width = p + 1;
    int const array_height = n + 1;
    int *const bin = (int *)malloc(array_height * array_width * sizeof(int));

    int i;
    for (i = 0; i < array_height; ++i) {

        int j;
        for (j = 0; j <= p && j <= i; ++j) {

            int const k = compute_index(array_width, i, j);

            if (j == i || 0 == j) {

                bin[k] = 1;
            } else {

                bin[k] = bin[compute_index(array_width, i - 1, j)] +
                         bin[compute_index(array_width, i - 1, j - 1)];
            }
        }
    }

    int const res = bin[compute_index(array_width, n, p)];
    free(bin);
    return res;
}
