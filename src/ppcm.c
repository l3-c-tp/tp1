#include "pgcd.h"
#include <stdio.h>
#include <stdlib.h>

int ppcm(int const argc, char *const argv[]) {

    if (argc == 1) {

        return atoi(argv[0]);
    }

    int const a = atoi(argv[0]);
    int const b = ppcm(argc - 1, &argv[1]);
    return a * b / pgcd(a, b);
}

int main(int argc, char *argv[]) {

    if (1 == argc) {

        printf("You need to add number(s) to arguments\n");
        return -1;
    }

    printf("%d\n", ppcm(argc - 1, &argv[1]));
    return 0;
}