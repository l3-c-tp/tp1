#ifndef __CANCRELESH_BINOMIAL__
#define __CANCRELESH_BINOMIAL__

int rec_binomial(int n, int p);

int rec_binomial_complexity(int n, int p, int *counter);

int array_binomial(int n, int p);

#endif