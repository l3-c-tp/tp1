# Advance C Programming TP1

This TP is divided in 4 exercises. To do them i used the Test Driven Development (TDD) methodology. 
The purpose of that kind of development is to write in first the tests, make them failed, and write the source code to pass the tests. 
This TP is available on my gitlab https://gitlab.com/users/Cancrelesh/groups on the L3 C TP group.


Some tests are written in Python because i wanted to use the subprocess module who is easy to use when you want to check the input and output in the terminal.

Those tests are run in a Python virtual environment.
To have the same venv as me you have the requirement.txt file.
```pip install pytest -c requirements.txt```

To check if each test in Python are pass,
- First connect to the venv with ```source .venv/bin/activate```.
- Then use the command ```pytest tests/```.
Normally the 8 tests are passed.
- To quit the venv use ```exit``` or ctrl+D.

For tests which are wrote in C i use cmocka framework.
Don't forget to Download it, or the C tests will not work and probably the compilation too.

The tests files are in /tests directory.
The sources files are in /src directory.
The includes files are in /include directory.


- To compile the three exercises:
```make```
When you use this command the /build directory is built and all executable are stock in.

- For exercise 1:

Type:
```./build/mytee```
Every thing you will write will be writing in the standard output until end of file (ctrl+D) or just use ctrl+C.

You can also write:
```./build/mytee name_of_file```
Like above, but everything will be also written in the file. If the file doesn't exist, he will be created.

- For exercise 2:

Type:
```./build/binomial```
You can enter two numbers and get their binomial value.

- For exercise 3:

Type:
```./build/ppcm number0 number1 number2 number3 ....```
This command will give you the less common multiple of those numbers.


- To compile the tests written in C:
```make tests```
When you use this command the /build directory, and the /build/tests directory are built, all tests executable are stock in build/tests.

````./build/tests/test_binomial```
Will print you "ok" if all the tests made for binomial.c are pass.

```./build/tests/test_pgcd```
Will print you "ok" if all the tests on the pgcd function are pass.


- For erasing all executable type:
```make clean```
build/ repo is removed and gmon.out too.
