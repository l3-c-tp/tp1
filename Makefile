
CC=gcc
CFLAGS=-ansi -Wall -pg 



all: build/mytee build/ppcm build/binomial

tests: build/tests/test_binomial build/tests/test_pgcd



build:
	mkdir -p build

build/objs:
	mkdir -p build/objs

build/tests:
	mkdir -p build/tests



build/mytee: src/mytee.c build
	$(CC) $(CFLAGS) src/mytee.c -o build/mytee

build/ppcm: src/ppcm.c build/objs/pgcd.o build
	$(CC) $(CFLAGS) -I include/ src/ppcm.c build/objs/pgcd.o -o build/ppcm

build/objs/pgcd.o: src/pgcd.c include/pgcd.h build/objs
	$(CC) $(CFLAGS) src/pgcd.c -c -o build/objs/pgcd.o

build/binomial: src/binomial.c build src/func_binomial.c include/func_binomial.h
	$(CC) $(CFLAGS) -I include/ src/binomial.c src/func_binomial.c -o build/binomial
	


build/tests/test_binomial: tests/test_binomial.c build/tests src/func_binomial.c include/func_binomial.h 
	$(CC) $(CFLAGS) -I include/ -o build/tests/test_binomial tests/test_binomial.c src/func_binomial.c -std=c99 -lcmocka

build/tests/test_pgcd: tests/test_pgcd.c build/tests build/objs/pgcd.o include/pgcd.h
	$(CC) $(CFLAGS) -I include/ -o build/tests/test_pgcd tests/test_pgcd.c build/objs/pgcd.o -std=c99 -lcmocka

build/tests/test_mocka: tests/test_mocka.c build/tests 
	$(CC) -std=c99 -o build/tests/test_mocka tests/test_mocka.c -lcmocka



clean:
	rm -rf build
	rm gmon.out